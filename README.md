## Name

# API starter-kit


## Description
This is a starter kit for a symfony API application documented with API Platform. It includes basic functionnalities like authentication, registration, forgot password


##  Installation 
```
git clone [git@gitlab.com:citeplus/starter-kit.git](git@gitlab.com:citeplus/starter-kit.git) || [https://gitlab.com/dupano/starter-kit.git](https://gitlab.com/citeplus/starter-kit.git)
composer install
php bin/console d:d:c
php bin/console m:migration
php bin/console d:m:m
php bin/console lexik:jwt:generate-keypair
symfony serve --port=8000 --no-tls

```


## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)

***


## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.


## License
Free