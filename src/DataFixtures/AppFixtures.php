<?php

namespace App\DataFixtures;

use App\Entity\Utilisateur;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $user = new Utilisateur();
        $user->setEmail('admin@site.com')
            ->setPassword('$2y$13$azYXGdzdxw7kCkKk3XRRPOOlio/BkxRrIKBpfKWzu.lTjqIF6w27i')//123
            ->setRoles(['ROLE_ADMIN'])
        ;
        $manager->persist($user);

        $manager->flush();
    }
}
