<?php
namespace App\Common;

class PaymentMode
{
    public const MANGO = 'MANGOPAY';
    public const PAYPAL = 'PAYPAL';
}
    