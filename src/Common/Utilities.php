<?php
namespace App\Common;

class Utilities
{
    /**
     * Get an object class name without the namespace or fail
     */
    public static function getEntityName($obj)
    {
        if(!is_object($obj))
            throw new \Exception("Error, attempting to get entity name of non object", 1);
        
        $final_array = explode('\\', get_class($obj));
        return end($final_array);
    } 

    public static function unaccent($string) 
    {
        $table = array(
            'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
            'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
            'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
            'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
            'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
            'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b',
            'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r',
        );
        
        return strtr($string, $table);
    }

    /**
     * Return a snake_case from a PascalCase of a string in input
     * @param string input
     * @return string
     */
    public static function snake_case(string $input): string 
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) 
        {
          $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }

        return implode('_', $ret);
    }

    public static function pluralize($string): string
    {
        // Liste de règles de formation du pluriel
        $pluralRules = array(
            '/(s|x|z)$/i' => '\1es',      // Ajouter "es" pour les mots se terminant par s, x, ou z
            '/([^aeiouy])y$/i' => '\1ies', // Remplacer le "y" par "ies" pour les mots se terminant par une consonne suivie d'un "y"
            '/$/' => 's'                   // Ajouter simplement "s" pour les autres cas
        );

        // Parcourir les règles et appliquer la première qui correspond à la chaîne donnée
        foreach ($pluralRules as $pattern => $replacement) 
        {
            if (preg_match($pattern, $string)) 
            {
                return preg_replace($pattern, $replacement, $string);
            }
        }

        return $string;
    }

    public static function snakeToPascal($string) 
    {
        $words = explode('_', $string);
    
        $pascalWords = array_map(function($word) {
            return ucfirst($word);
        }, $words);
    
        $pascalString = lcfirst(implode('', $pascalWords));

        return $pascalString;
    }



    public static function hydrateJsonLd(mixed $element)
    {
        $object = [];
        if(is_array($element))
        {
            foreach($element as $key => $value)
            {
                if(in_array($key, ['password']))
                    continue;

                $object[self::snakeToPascal($key)] = $value;
            }            
        }

        return $object;
    }

}