<?php

namespace App\Common;

class Blacklist {
    private $filePath;
    public $JSON_FOLDER_PATH ;

    public function __construct(private $sentence, private $sentenceToSend = null, private $listFile = [], private $wordsDict = null) {
        $this->JSON_FOLDER_PATH = getcwd()."/json/";
    }

    public function splitSentence() {
        $words = explode(' ', $this->sentence);
        $this->wordsDict = [];
        foreach ($words as $word) {
            $this->wordsDict[] = [$word, false]; // When it's true, it means the word is in one file
        }
        return $this->wordsDict;
    }

    public function getListJsonFile() {
        $this->JSON_FOLDER_PATH; //dd();
        foreach (scandir($this->JSON_FOLDER_PATH) as $file) {
            if (is_file($this->JSON_FOLDER_PATH . $file)) {
                $this->listFile[] = $file;
            }
        }
        return $this->listFile;
    }

    public function loadJsonContentFile($jsonFile) {
        //global $JSON_FOLDER_PATH;
        $jsonContent = file_get_contents($this->JSON_FOLDER_PATH . $jsonFile);
        return json_decode($jsonContent, true);
    }

    public function search() {
        $wordToRemove = [];
        foreach ($this->getListJsonFile() as $jsonFile) {
            $parsedJson = $this->loadJsonContentFile($jsonFile);
            foreach ($parsedJson as $word => $value) {
                if (strpos($this->sentence, $value) !== false) {
                    $wordToRemove[] = $value;
                }
            }
        }
        return $wordToRemove;
    }

    public function removeWord() {
        foreach ($this->search() as $word) {
            $temp = explode($word, $this->sentence);
            $this->sentence = implode(' ', $temp);
        }
        return $this->sentence;
    }
}

/*$objet = new Blacklist("Ceci est une phrdeuxase.");
echo $objet->removeWord();*/  // Affiche "Ceci est une phrase."