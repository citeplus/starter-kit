<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Controller\AuthController;
use App\Repository\UtilisateurRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\UuidV7 as Uuid;
use ApiPlatform\OpenApi\Model;


#[ORM\Entity(repositoryClass: UtilisateurRepository::class)]
#[ApiResource(
    normalizationContext: ['groups'=> ['Utilisateur:read']],
    denormalizationContext: ['groups'=> ['Utilisateur:write', 'Site:write']],
)]

#[GetCollection(
    normalizationContext:  ['groups'=> ['Utilisateur:read']]
)]

#[Post()]

#[Get (
    normalizationContext:  ['groups'=> ['Utilisateur:read']],
)]

#[Get(
    name: 'app_me', 
    uriTemplate: '/api/me',
    controller: AuthController::class, 
    validationContext: ['groups' => ['Default']], 
    openapi: new Model\Operation(
        operationId: 'whoami',
        tags: ['Whoami'],
        summary: 'Whoami',
        requestBody: new Model\RequestBody(
            description: 'Detail on who is connected'
        ),
        
    ),security: "is_granted('ROLE_ADMIN')",
)]

#[Post(
    name: 'app_user_change_password', 
    routeName: 'app_user_change_password',
    uriTemplate: 'api/users/change-password', 
    controller: AuthController::class, 
    openapi: new Model\Operation(
        summary: 'Change password', 
        description: 'Change le mot de passe pour un utilisateur', 
        requestBody: new Model\RequestBody(
            content: new \ArrayObject([
                'application/json' => [
                    'schema' => [
                        'type' => 'object', 
                        'properties' => [
                            'currentPassword' => [
                                'type' => 'string'
                            ],
                            'password' => [
                                'type' => 'string'
                            ],
                            'passwordConfirm' => [
                                'type' => 'string'
                            ],
                        ]
                    ]
                ]
            ])
        )
    ),security: "is_granted('ROLE_ADMIN')",
)]   
#[Put]

#[Patch]

#[Delete]

class Utilisateur implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\Column(type: UuidType::NAME, unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    #[Groups(['Utilisateur:read'])]
    private ?Uuid $id = null;

    #[ORM\Column(length: 180, unique: true)]
    #[Groups(['Utilisateur:read', 'Utilisateur:write'])]
    private ?string $email = null;

    #[ORM\Column]
    #[Groups(['Utilisateur:read', 'Utilisateur:write'])]
    private array $roles = [];

    /**
     * @var string The hashed password
     */
    #[ORM\Column]
    #[Groups(['Utilisateur:write'])]
    private ?string $password = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['Utilisateur:read', 'Utilisateur:write'])]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['Utilisateur:read', 'Utilisateur:write'])]
    private ?\DateTimeImmutable $updatedAt = null;


    public function __construct()
    {
        $this->id = Uuid::v7();
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): static
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): static
    {
        $this->password = $password;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }
}
