<?php

namespace App\Controller;

use App\Entity\Utilisateur;
use App\Repository\UtilisateurRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\SecurityBundle\Security;

class AuthController extends AbstractController
{
    public function __construct(private EntityManagerInterface $entityManager, 
        private UtilisateurRepository $utilisateurRepository, 
        private UserPasswordHasherInterface $passwordHasher,
        private SerializerInterface $serializer,
        private Security $security)
    {

    }

    #[Route('/api/register', name: 'app_registration', methods: ["POST"])]
    public function create(Request $request): JsonResponse
    {
        $data = $request->request->all();

        $this->entityManager->getConnection()->beginTransaction();
        try {
            $user = new Utilisateur();
            $hashedPassword = $this->passwordHasher->hashPassword(
                $user,
                $request->request->get('password')
            );
            $user->setEmail($data['email'])
                ->setPassword($hashedPassword)
                ;

            $message = [];
            $found = false;
            $userExist = $this->utilisateurRepository->findOneBy(["email" => $data['email']]);
            if ($userExist != null) {
                $found = true;
                $message["email"] = "Cette adresse mail est déjà utilisée";
            }
           
            if ($found) {
                return $this->json([
                    "code" => Response::HTTP_CONFLICT,
                    "message" => $message
                ], Response::HTTP_CONFLICT);
            }
            $user->setRoles($this->mapRole($data['roles'] ?? null));

            $this->utilisateurRepository->save($user, true);
            
            $this->entityManager->getConnection()->commit();

            $u = json_decode($this->serializer->serialize($user, 'json', ['groups' => ['Utilisateur:read']]), true);

            return $this->json($u, Response::HTTP_CREATED);

        } catch (\Exception $e) {
            $this->entityManager->getConnection()->rollBack();
            return $this->json([
                "code" => Response::HTTP_CONFLICT, "message" => "Une erreur s'est produite : ".$e->getMessage()
            ], Response::HTTP_CONFLICT);
        }
    }

    #[Route('/login', name:"app_login")]
    public function login(AuthenticationUtils $authenticationUtils)
    {
        return $this->render('auth/login.html.twig', [
            'last_username' => $authenticationUtils->getLastUsername(),
            'error' => $authenticationUtils->getLastAuthenticationError(),
            'app_name' => $_ENV['APP_NAME'] ?? '',
        ]);
    }

    #[Route('/logout', name: 'app_logout')]
    public function logout()
    {
        
    }
    // #[Route('/api/logout', name: 'app_logout_api')]
    // public function logoutApi()
    // {
    //     // logout the user in on the current firewall
    //     $response = $this->security->logout(false);
    //     return $this->json(["code"=>Response::HTTP_OK]);
    // }

    #[Route('/api/me', name: 'app_user_info', methods: ['GET'])]
    public function me(): Response
    {
        $this->denyAccessUnlessGranted("IS_AUTHENTICATED");
        $u = json_decode($this->serializer->serialize($this->getUser(), 'json', ['groups' => ['Utilisateur:read']]), true);
        return new JsonResponse(array_merge([
            "@context" => "/api/contexts/Utilisateur",
            "@id" => "/api/utilisateurs/" . $u["id"],
            "@type" => "Utilisateur",
        ], $u));
    }

    public function mapRole(mixed $value):array
    {
        if($value == null)
            return ['ROLE_USER'];
        if(!is_array($value))
        {
            $value = strtoupper($value);
            if(str_contains($value, 'ROLE_'))
                return (array)$value;
            else
                return (array)('ROLE_'.$value);
        }
        return $value;
    }
    #[Route('api/users/change-password', name: 'app_user_change_password', methods: ["POST"])]
    public function changePassword(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED');
        $user = $this->getUser();
        $this->entityManager->getConnection()->beginTransaction();
        try {
        if(!$user instanceof Utilisateur)
            return $this->json(['code' => Response::HTTP_UNAUTHORIZED, 'message' => 'Vous n\êtes pas correctement authentifié. '], Response::HTTP_UNAUTHORIZED);

        //check user password
        $data = json_decode($request->getContent(), true);
        if (!$this->passwordHasher->isPasswordValid($user, $data['currentPassword']))
            return $this->json(['code' => Response::HTTP_FORBIDDEN, 'message' => ['message' => 'Votre mot de passe est incorrect']], Response::HTTP_FORBIDDEN);

        if ($data['password'] != $data['confirmPassword'])
            return $this->json(['code' => Response::HTTP_FORBIDDEN, 'message' => ['message' => 'Les mots de passe ne correespondent pas']], Response::HTTP_FORBIDDEN);

        if ($this->passwordHasher->isPasswordValid($user, $data['password']))
            return $this->json(['code' => Response::HTTP_FORBIDDEN, 'message' => ['message' => 'Le nouveau mot de passe doit être différent du mot de passe actuel.']], Response::HTTP_FORBIDDEN);

        $regex = "/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-\.]).{8,}$/";

        if (!preg_match($regex, $data['password']))
            return $this->json(['code' => Response::HTTP_FORBIDDEN, 'message' => ['message' => 'Le mot de passe doit contenir au moins 8 caractères, une majuscule et un caractère spécial']], Response::HTTP_CONFLICT);

        $hashedPassword = $this->passwordHasher->hashPassword($user, $data['password']);

        $user->setPassword($hashedPassword);

        $this->utilisateurRepository->save($user, true);
            
        $this->entityManager->getConnection()->commit();
        return $this->json(["code"=>Response::HTTP_OK]);
    } catch (\Exception $e) {
        $this->entityManager->getConnection()->rollBack();
        return $this->json([
            "code" => Response::HTTP_CONFLICT, "message" => "Une erreur s'est produite : ".$e->getMessage()
        ], Response::HTTP_CONFLICT);
    }
    }
}
