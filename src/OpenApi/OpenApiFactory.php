<?php
namespace App\OpenApi;

use ApiPlatform\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\OpenApi\OpenApi;
use ApiPlatform\OpenApi\Model;

class OpenApiFactory implements OpenApiFactoryInterface
{

    public function __construct(private OpenApiFactoryInterface $decorated)
    {
    }

    public function __invoke(array $context = []): OpenApi
    {
        $openApi = $this->decorated->__invoke($context);
        
        //$openApi = ($this->decorated)($context);
        $schemas = $openApi->getComponents()->getSchemas();


        $schemas['Register'] = new \ArrayObject([
            'type' => 'object',
            'properties' => [
                'email' => [
                    'type' => 'string',
                    'example' => 'johndoe@example.com',
                ],
                'password' => [
                    'type' => 'string',
                    'example' => 'apassword',
                ],
                'roles[]' => [
                    'type' => 'string',
                    'example' => '',
                ],
            ],
        ]);

        $pathItem = new Model\PathItem(
            ref: 'Create User',
            post: new Model\Operation(
                operationId: 'postCredentialsRefreshToken',
                tags: ['Register'],
                responses: [
                    '201' => [
                        'description' => 'User created',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/Register',
                                ],
                            ],
                        ],
                    ],
                ],
                summary: 'Create user',
                requestBody: new Model\RequestBody(
                    description: 'Create new User',
                    content: new \ArrayObject([
                        'multipart/form-data' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/Register',
                            ],
                        ],
                    ]),
                ),
                security: [],
            ),
        );
        $openApi->getPaths()->addPath('/api/register', $pathItem);


        //add refresh Token path
        $pathItem = new Model\PathItem(
            ref: 'Refresh user token',
            post: new Model\Operation(
                operationId: 'postCredentialsItem',
                tags: ['RefreshToken'],
                responses: [
                    '200' => [
                        'description' => 'New token',
                        'content' => [
                            'application/json' => [
                                'schema' => new \ArrayObject([
                                    'type' => 'object',
                                    'properties' => [
                                        'token' => [
                                            'type' => 'string',
                                        ],
                                        'refresh_token' => [
                                            'type' => 'string',
                                        ],
                                    ],
                                ]),
                            ],
                        ],
                    ],
                ],
                summary: 'Refresh user token',
                requestBody: new Model\RequestBody(
                    description: 'Refresh user token from a refresh_token input',
                    content: new \ArrayObject([
                        'application/json' => [
                            'schema' => new \ArrayObject([
                                'type' => 'object',
                                'properties' => [
                                    'refresh_token' => [
                                        'type' => 'string',
                                    ],
                                ],
                            ]),
                        ],
                    ]),
                ),
                security: [],
            ),
        );

        $openApi->getPaths()->addPath('/token/refresh', $pathItem);       
       
        // to define base path URL
        //$openApi = $openApi->withServers([new Model\Server('https://localhost:9090')]);

        return $openApi;
    }
}